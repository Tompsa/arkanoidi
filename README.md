Simple Arkanoid clone made with SFML 2.3/C++.

Since project/solution is configured to use dynamic libraries, you need to copy .dll files from SFML installation directory's bin folder to Debug (or Release, depending on which version you are building) folder, where the executables are created.