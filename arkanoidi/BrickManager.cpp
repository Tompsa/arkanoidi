#include "stdafx.h"
#include "BrickManager.h"
#include "Brick.h"

BrickManager::BrickManager()
{

}

BrickManager::~BrickManager()
{
	std::for_each(_bricks.begin(), _bricks.end(), BrickDeallocator());
}

void BrickManager::Add(Brick* brickObject)
{
	_bricks.push_back(brickObject);
}

int BrickManager::GetObjectCount() const
{
	return _bricks.size();
}

void BrickManager::DrawAll(sf::RenderWindow& renderWindow)
{
	std::vector<Brick*>::const_iterator itr = _bricks.begin();
	while (itr != _bricks.end())
	{
		if (!(*itr)->isDestroyed())
		{
			(*itr)->Draw(renderWindow);
			itr++;
		}
		else
			itr++;
	}
}

void BrickManager::UpdateAll()
{
	std::vector<Brick*>::const_iterator itr = _bricks.begin();

	float timeDelta = clock.restart().asSeconds();

	while (itr != _bricks.end())
	{
		(*itr)->Update(timeDelta);
		itr++;
	}
}

const std::vector<Brick*>& BrickManager::GetBricks() const
{
	return _bricks;
}