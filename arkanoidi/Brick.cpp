#include "stdafx.h"
#include "Brick.h"

Brick::Brick(int x, int y) :
destroyed(false)
{
	Load("images/brick.png");
	assert(IsLoaded());

	//GetSprite().setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);
	this->SetPosition(x, y);
}

bool Brick::isDestroyed()
{
	return destroyed;
}

void Brick::setDestroyed(bool val)
{
	destroyed = val;
}