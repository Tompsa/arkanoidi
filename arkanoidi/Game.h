#ifndef GAME_H
#define GAME_H

#include "SFML\Window.hpp"
#include "SFML\Graphics.hpp"
#include "SFML\Audio.hpp"
#include "PlayerPaddle.h"
#include "GameBall.h"
#include "GameObjectManager.h"
#include "BrickManager.h"
#include "Brick.h"


class Game
{
public:
	static void Start();
	static sf::RenderWindow& GetWindow();
	const static GameObjectManager& GetGameObjectManager();
	const static BrickManager& GetBrickManager();
	const static int SCREEN_WIDTH = 300;
	const static int SCREEN_HEIGHT = 400;
	const static int N_OF_BRICKS = 30;
	const static int INIT_PADDLE_X = 200;
	const static int INIT_PADDLE_Y = 360;
	const static int INIT_BALL_X = 230;
	const static int INIT_BALL_Y = 355;
	const static int DELAY = 1000;
	const static int PERIOD = 10;

private:
	static bool IsExiting();
	static void GameLoop();

	static void ShowSplashScreen();
	static void ShowMenu();

	enum GameState {
		Uninitialized, ShowingSplash, Paused,
		ShowingMenu, Playing, Exiting
	};

	static GameState _gameState;
	static sf::RenderWindow _mainWindow;

	static GameObjectManager _gameObjectManager;
	static BrickManager _brickManager;
};


#endif // GAME_H