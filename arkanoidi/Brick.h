#ifndef BRICK_H
#define BRICK_H

#include "VisibleGameObject.h"

class Brick : 
	public VisibleGameObject
{
public: 
	Brick(int x, int y);

	bool isDestroyed();
	void setDestroyed(bool val);

private:
	bool destroyed;
};

#endif