#ifndef BRICKMANAGER_H
#define BRICKMANAGER_H

#include "GameObjectManager.h"
#include "Brick.h"

class BrickManager :
	public GameObjectManager
{
public:
	BrickManager();
	~BrickManager();

	void Add(Brick* brickObject);
	int GetObjectCount() const;
	const std::vector<Brick*>& GetBricks() const;

	void DrawAll(sf::RenderWindow& renderWindow);
	void UpdateAll();

private:
	std::vector<Brick*> _bricks;

	sf::Clock clock;

	struct BrickDeallocator
	{
		void operator()(const Brick* p) const
		{
			delete p;
		}
	};
};

#endif