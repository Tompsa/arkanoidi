#ifndef VISIBLEGAMEOBJECT_H
#define VISIBLEGAMEOBJECT_H

#include "VisibleGameObject.h"

class PlayerPaddle :
	public VisibleGameObject
{
public:
	PlayerPaddle();
	~PlayerPaddle();
    
	void Update(float elapsedTime);
    void Draw(sf::RenderWindow& rw);
    
	void AddVelocity(float var);
    float GetVelocity() const;

private:
    float _velocity;  // -- left ++ right
    float _maxVelocity;
};

#endif //VISIBLEGAMEOBJECT_H